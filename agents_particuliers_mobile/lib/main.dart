import 'package:agents_particuliers_mobile/Components/AddProperty/AddProperty.dart';
import 'package:agents_particuliers_mobile/Components/ListProperty/listProperties.dart';
import 'package:agents_particuliers_mobile/Components/ViewProperty/viewProperty.dart';
import 'package:flutter/material.dart';
import 'Components/ConnectionScreen/ConnectionScreen.dart';
import 'Components/RegisterScreen/RegisterScreen.dart';
import 'Components/Profile/Profile.dart';
import 'Components/Profile/Components/TakePictureScreenProfile/TakePictureScreenProfile.dart';
import 'Components/Profile/Components/Favoris/Favoris.dart';
import 'Components/AddProperty/Components/TakePictureAddPropertyScreen/TakePictureAddPropertyScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  TakePictureScreen takePicture;
  TakePictureAddPropertyScreen takePictureAddProperty;
  Profile profile;
  ConnectionScreen connection;
  ListProperties list;
  ViewProperty view;
  AddProperty add;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    this.takePicture = new TakePictureScreen();
    this.profile = new Profile(this.takePicture);
    this.takePicture.setProfile(this.profile);
    this.connection = new ConnectionScreen(profile);
    this.view = new ViewProperty(profile);
    this.list = new ListProperties(profile, this.view);
    this.takePictureAddProperty = new TakePictureAddPropertyScreen();
    this.add = new AddProperty(this.takePictureAddProperty, profile);
    this.takePictureAddProperty.setAddProperty(this.add);

    return MaterialApp(
      home: this.list,
      routes: {
        "/Register": (_) => new RegisterScreen(profile),
        "/Connection": (_) => this.connection,
        "/Profile": (_) => profile,
        "/TakePicture": (_) => takePicture,
        "/TakePictureAddProperty": (_) => takePictureAddProperty,
        "/Favoris": (_) => new Favoris(profile),
        "/ListProperty": (_) => this.list,
        "/ViewProperty": (_) => this.view,
        "/AddProperty": (_) => this.add,
      },
    );
  }
}
