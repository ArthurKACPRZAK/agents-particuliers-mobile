import 'package:flutter/material.dart';

class Input extends StatelessWidget {

Input(this.label, this.password);

final String label;

final bool password;

@override
  Widget build(BuildContext context) {
    return 
            Container(
              width: MediaQuery.of(context).size.width * 0.6,
              child: TextField(
                obscureText: this.password,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: label,
                ),
              ),
              padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
            );
  }

}