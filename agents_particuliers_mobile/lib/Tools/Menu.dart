import 'package:flutter/material.dart';
import 'package:agents_particuliers_mobile/Components/Profile/Profile.dart';

class Menu extends StatelessWidget {
  final Profile profile;

  Menu(this.profile);

  @override
  Widget build(BuildContext context) {
    var Connect = <Widget>[
      DrawerHeader(
        child: Text('Rent My House Logo',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            )),
        decoration: BoxDecoration(
          color: Colors.black,
        ),
      ),
      ListTile(
        title: Row(children: <Widget>[
          Icon(
            Icons.home,
            color: Colors.grey,
          ),
          Text(" Profile")
        ]),
        onTap: () {
          Navigator.pop(context);
          Navigator.pushReplacementNamed(context, "/Profile");
        },
      ),
      ListTile(
        title: Row(children: <Widget>[
          Icon(
            Icons.home,
            color: Colors.grey,
          ),
          Text(" List")
        ]),
        onTap: () {
          Navigator.pop(context);
          Navigator.pushReplacementNamed(context, "/ListProperty");
        },
      ),
      ListTile(
        title: Row(children: <Widget>[
          Icon(
            Icons.add,
            color: Colors.grey,
          ),
          Text(" Add Property")
        ]),
        onTap: () {
          Navigator.pop(context);
          Navigator.pushReplacementNamed(context, "/AddProperty");
        },
      ),
    ];

    var NoConnect = <Widget>[
      DrawerHeader(
        child: Text('Rent My House Logo',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            )),
        decoration: BoxDecoration(
          color: Colors.black,
        ),
      ),
      ListTile(
        title: Row(children: <Widget>[
          Icon(
            Icons.favorite,
            color: Colors.grey,
          ),
          Text(" Connection")
        ]),
        onTap: () {
          Navigator.pop(context);
          Navigator.pushReplacementNamed(context, "/Connection");
        },
      ),
      ListTile(
        title: Row(children: <Widget>[
          Icon(
            Icons.home,
            color: Colors.grey,
          ),
          Text(" Register")
        ]),
        onTap: () {
          Navigator.pop(context);
          Navigator.pushReplacementNamed(context, "/Register");
        },
      ),
      ListTile(
        title: Row(children: <Widget>[
          Icon(
            Icons.home,
            color: Colors.grey,
          ),
          Text(" List")
        ]),
        onTap: () {
          Navigator.pop(context);
          Navigator.pushReplacementNamed(context, "/ListProperty");
        },
      ),
    ];

    var tmp = NoConnect;

    if (this.profile.id != "-1") {
      tmp = Connect;
    }

    return Drawer(
      child: ListView(padding: EdgeInsets.zero, children: tmp),
    );
  }
}
