import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:agents_particuliers_mobile/Tools/Menu.dart';
import 'package:requests/requests.dart';
import 'package:camera/camera.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import './Components/TakePictureScreenProfile/TakePictureScreenProfile.dart';
import 'dart:convert';
import 'dart:io';

class Profile extends StatefulWidget {
  String id = "-1";

  final TakePictureScreen takePicture;

  String imagePath = "";

  Profile(this.takePicture);

  void setId(String id) {
    this.id = id;
  }

  void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  @override
  _Profile createState() => _Profile();
}

class _Profile extends State<Profile> {
  List<CameraDescription> cameras;

  var firstCamera;

  bool _isReady = false;

  String imageTmp = "";

  @override
  void initState() {
    super.initState();
    _setupCameras();

    this.getImage(this.widget.id);
  }

  void _setupCameras() async {
    try {
      cameras = await availableCameras();

      firstCamera = cameras.first;
    } on CameraException catch (_) {}
    if (!mounted) return;
    setState(() {
      _isReady = true;
    });
  }

  void addImage(String id, String base64) async {
    print("add");
    print(base64.length);
    var url =
        'https://agentsparticuliers.com/index.php/Registercontroller/addImage';

    var response =
        await Requests.post(url, body: {'image': '$base64', 'id': id});

    if (response.statusCode == 200) {
      print("ok");
    } else {
      print('add Request failed with status: ${response.statusCode}.');
    }
  }

  void getImage(String id) async {
    print("get");
    print(id);
    var url =
        'http://agentsparticuliers.com/index.php/Registercontroller/getImage?id=$id';

    var response = await http.get(url);
    if (response.statusCode == 200) {
      print("end");
      var jsonResponse = convert.jsonDecode(response.body);
      if (jsonResponse != null && !jsonResponse.isEmpty) {
        if (jsonResponse[0]['image'] != null) {
          print("end");

          this.setState(() {
            this.imageTmp = jsonResponse[0]['image'];
          });
        }
      }
    } else {
      print('get Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_isReady) {
      Image img;

      if (this.widget.imagePath == "") {
        if (this.imageTmp == "") {
          img = new Image(image: AssetImage('Assets/photo.png'));
        } else {
          print("adzazazfaef");
          print(this.imageTmp.length);
          Uint8List bytes = base64Decode(this.imageTmp);

          img = new Image.memory(bytes);
        }
      } else {
        File imagefile = new File(this.widget.imagePath);

        List<int> imageBytes = imagefile.readAsBytesSync();
        String base64Image = base64Encode(imageBytes);

        this.addImage(this.widget.id, base64Image);

        Uint8List bytes = base64Decode(base64Image);

        img = new Image.memory(bytes);
      }

      return Scaffold(
          body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 100.0,
                    height: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      color: Colors.redAccent,
                    ),
                    child: img,
                  ),
                  TextButton(
                      child: Text('Take picture'),
                      onPressed: () {
                        widget.takePicture.setCamera(firstCamera, "/Profile");
                        Navigator.pushReplacementNamed(context, "/TakePicture");
                      }),
                  Container(
                    margin: const EdgeInsets.only(bottom: 30.0),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Row(children: <Widget>[
                            Icon(
                              Icons.favorite,
                            ),
                            Text(" Favoris")
                          ]),
                          onTap: () {
                            Navigator.pushReplacementNamed(context, "/Favoris");
                          },
                        ),
                        ListTile(
                          title: Row(children: <Widget>[
                            Icon(
                              IconData(59823, fontFamily: 'MaterialIcons'),
                            ),
                            Text(" Annonces vues (0)")
                          ]),
                          onTap: () {
                            Navigator.pushReplacementNamed(context, "/Favoris");
                          },
                        ),
                        ListTile(
                          title: Row(children: <Widget>[
                            Icon(
                              IconData(59823, fontFamily: 'MaterialIcons'),
                            ),
                            Text(" Annonces déposées (0)")
                          ]),
                          onTap: () {
                            Navigator.pushReplacementNamed(context, "/Favoris");
                          },
                        ),
                        ListTile(
                          title: Row(children: <Widget>[
                            Icon(
                              IconData(59148, fontFamily: 'MaterialIcons'),
                            ),
                            Text(" Se déconnecter")
                          ]),
                          onTap: () {
                            Navigator.pushReplacementNamed(context, "/Favoris");
                          },
                        ),
                      ],
                    ),
                  ),
                ]),
          ),
          drawer: Menu(this.widget));
    }
    return Container();
  }
}
