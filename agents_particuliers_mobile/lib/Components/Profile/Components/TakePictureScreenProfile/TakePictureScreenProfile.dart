import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import '../../Profile.dart';
import 'dart:async';

class TakePictureScreen extends StatefulWidget {
  CameraDescription camera;
  String recall = "";

  Profile profile;

  void setProfile(Profile profile) {
    this.profile = profile;
  }

  void setCamera(CameraDescription camera, String recall) {
    this.camera = camera;
    this.recall = recall;
  }

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();

    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );

    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Take a picture')),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(_controller);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        onPressed: () async {
          try {
            await _initializeControllerFuture;

            // Attempt to take a picture and get the file `image`
            final image = await _controller.takePicture();

            this.widget.profile.setImagePath(image?.path);

            Navigator.pushReplacementNamed(context, this.widget.recall);
          } catch (e) {
            print(e);
          }
        },
      ),
    );
  }
}
