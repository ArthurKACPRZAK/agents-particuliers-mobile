import 'package:flutter/material.dart';
import 'package:agents_particuliers_mobile/Tools/Menu.dart';
import 'package:agents_particuliers_mobile/Components/Profile/Profile.dart';

class Favoris extends StatelessWidget {
  final Profile profile;

  Favoris(this.profile);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Search")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[Text("Favoris")],
          ),
        ),
        drawer: Menu(this.profile));
  }
}
