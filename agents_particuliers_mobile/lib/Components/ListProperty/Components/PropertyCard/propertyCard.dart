import 'dart:convert';
import 'dart:typed_data';

import 'package:agents_particuliers_mobile/Components/ViewProperty/viewProperty.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PropertyCard extends StatefulWidget {
  String Price = "";
  String Addr = "";
  String Bed = "";
  String Bath = "";
  String Parking = "";
  String image = "";

  final ViewProperty view;
  PropertyCard(this.Price, this.Addr, this.Bed, this.Bath, this.Parking,
      this.image, this.view);

  @override
  _PropertyCardState createState() => _PropertyCardState();
}

class _PropertyCardState extends State<PropertyCard> {
  @override
  Widget build(BuildContext context) {
    Uint8List bytes = base64Decode(this.widget.image);

    Image img = new Image.memory(bytes);
    return SizedBox(
      width: double.infinity,
      child: Card(
        child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            onTap: () {
              this.widget.view.setAll(this.widget.Price, this.widget.Addr,
                  this.widget.Bed, this.widget.Bath, this.widget.Parking, img);
              Navigator.pushReplacementNamed(context, "/ViewProperty");
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    alignment: Alignment.topLeft, height: 200.0, child: img),
                Container(
                  margin: const EdgeInsets.only(top: 15.0, left: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(this.widget.Price,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22)),
                      Padding(
                          padding: const EdgeInsets.only(
                            left: 15,
                          ),
                          child: Text('Studio Apartment'))
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10.0, left: 10),
                  child: Text(
                    this.widget.Addr,
                    style: TextStyle(
                      color: Colors.blueGrey,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  child: Divider(
                    height: 20,
                    thickness: 1,
                    indent: 10,
                    endIndent: 10,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 5,
                    bottom: 15,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Icon(
                        Icons.hotel,
                        color: Colors.blue,
                        size: 24.0,
                      ),
                      Text(this.widget.Bed + ' Bed'),
                      Icon(
                        Icons.local_laundry_service,
                        color: Colors.blue,
                        size: 24.0,
                      ),
                      Text(this.widget.Bath + ' Bath'),
                      Icon(
                        Icons.local_taxi,
                        color: Colors.blue,
                        size: 24.0,
                      ),
                      Text(this.widget.Parking + ' parking'),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
