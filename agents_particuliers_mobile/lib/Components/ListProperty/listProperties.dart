import 'package:agents_particuliers_mobile/Components/Profile/Profile.dart';
import 'package:agents_particuliers_mobile/Components/ViewProperty/viewProperty.dart';
import 'package:agents_particuliers_mobile/Tools/Menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import './components/PropertyCard/propertyCard.dart';

class ListProperties extends StatefulWidget {
  final Profile profile;
  final ViewProperty view;

  ListProperties(this.profile, this.view);

  @override
  _ListPropertiesState createState() => _ListPropertiesState();
}

class _ListPropertiesState extends State<ListProperties> {
  List<Widget> list = [];
  void getList() async {
    var url =
        'http://agentsparticuliers.com/index.php/Registercontroller/getList';

    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      if (!jsonResponse.isEmpty) {
        print(response.body);
        var l = jsonResponse;
        print(l.length);
        print('ok');
        this.setState(() {
          for (var i = 0; i < l.length; i++) {
            this.list.add(PropertyCard(
                l[i]['price'],
                l[i]['addr'],
                l[i]['bed'],
                l[i]['bath'],
                l[i]['parking'],
                l[i]['image'],
                this.widget.view));
          }
        });
      }
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  void initState() {
    super.initState();
    this.getList();
  }

  @override
  Widget build(BuildContext context) {
    print(this.list.length);
    if (this.list.length == 0) {
      return Container();
    }
    return Scaffold(
        appBar: AppBar(title: Text("ListProperties")),
        body: Container(
          child: Padding(
            padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
            child: ListView(
              children: this.list,
            ),
          ),
        ),
        drawer: Menu(this.widget.profile));
  }
}
