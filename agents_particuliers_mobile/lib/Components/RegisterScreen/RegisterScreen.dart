import 'package:agents_particuliers_mobile/Components/RegisterScreen/Components/Input/Input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:agents_particuliers_mobile/Tools/Menu.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:agents_particuliers_mobile/Components/Profile/Profile.dart';

class RegisterScreen extends StatelessWidget {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final Profile profile;

  RegisterScreen(this.profile);

  void addUser(String email, String pass, BuildContext context) async {
    var url =
        'http://agentsparticuliers.com/index.php/Registercontroller/addUser?email=$email&password=$pass';

    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var id = jsonResponse[0]['id'];
      this.profile.setId(id);
      Navigator.pushReplacementNamed(context, "/Profile");
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [Colors.white, Color.fromRGBO(207, 207, 241, 1)])),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(bottom: 30.0),
                      child: Text('RentMyHouse',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 23)),
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 10.0),
                      child: Text('Let\'s Register.',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Roboto',
                              fontSize: 34)),
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 100.0),
                      child: Text(
                        'Don\'t have an account? Register',
                        style: TextStyle(fontFamily: 'Roboto', fontSize: 14),
                      ),
                    ),
                    Input("Firstname", this.firstNameController),
                    Input("Lastname", this.lastNameController),
                    Input("Email", this.emailController),
                    Input("Password", this.passwordController),
                    SizedBox(
                      width: double.infinity,
                      child: FlatButton(
                          onPressed: () {
                            this.addUser(emailController.text,
                                passwordController.text, context);
                          },
                          child: Text(
                            'Register',
                            style: TextStyle(fontSize: 16),
                          ),
                          color: Color.fromRGBO(33, 8, 174, 1),
                          textColor: Colors.white),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        drawer: Menu(this.profile));
  }
}
