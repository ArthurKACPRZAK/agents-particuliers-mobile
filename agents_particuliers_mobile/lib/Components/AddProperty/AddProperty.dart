import 'dart:typed_data';
import 'package:agents_particuliers_mobile/Components/AddProperty/Components/ValidationButton/ValidationButton.dart';
import 'package:agents_particuliers_mobile/Components/Profile/Profile.dart';
import 'package:flutter/material.dart';
import 'package:agents_particuliers_mobile/Tools/Menu.dart';
import './Components/Input/Input.dart';
import 'package:requests/requests.dart';
import 'package:camera/camera.dart';
import 'dart:convert';
import 'dart:io';
import './Components/TakePictureAddPropertyScreen/TakePictureAddPropertyScreen.dart';

class AddProperty extends StatefulWidget {
  final TakePictureAddPropertyScreen takePicture;
  final Profile profile;

  String imagePath = "";

  AddProperty(this.takePicture, this.profile);

  void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  @override
  _AddProperty createState() => _AddProperty();
}

class _AddProperty extends State<AddProperty> {
  List<CameraDescription> cameras;

  var firstCamera;

  bool _isReady = false;

  String imageTmp = "";
  String base64Image = "";

  final priceController = TextEditingController();
  final addressController = TextEditingController();
  final parkingController = TextEditingController();
  final bedController = TextEditingController();
  final bathController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _setupCameras();
  }

  void _setupCameras() async {
    try {
      cameras = await availableCameras();

      firstCamera = cameras.first;
    } on CameraException catch (_) {}
    if (!mounted) return;
    setState(() {
      _isReady = true;
    });
  }

  void addProps(String id, String base64, String price, String addr, String bed,
      String bath, String parking) async {
    print("add");

    var url =
        'https://agentsparticuliers.com/index.php/Registercontroller/addProps';

    var response = await Requests.post(url, body: {
      'image': '$base64',
      'id': id,
      'price': price,
      'addr': addr,
      'bed': bed,
      'bath': bath,
      'parking': parking
    });

    if (response.statusCode == 200) {
      Navigator.pushReplacementNamed(context, "/Profile");
    } else {
      print('add Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_isReady) {
      Image img;

      if (this.widget.imagePath == "") {
        if (this.imageTmp == "") {
          img = new Image(image: AssetImage('Assets/photo.png'));
        } else {
          Uint8List bytes = base64Decode(this.imageTmp);

          img = new Image.memory(bytes);
        }
      } else {
        File imagefile = new File(this.widget.imagePath);

        List<int> imageBytes = imagefile.readAsBytesSync();
        base64Image = base64Encode(imageBytes);

        Uint8List bytes = base64Decode(base64Image);

        img = new Image.memory(bytes);
      }

      return Scaffold(
          body: Center(
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      height: 150.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        color: Colors.redAccent,
                      ),
                      child: img,
                    ),
                    TextButton(
                        child: Text('Take picture'),
                        onPressed: () {
                          widget.takePicture
                              .setCamera(firstCamera, "/AddProperty");
                          Navigator.pushReplacementNamed(
                              context, "/TakePictureAddProperty");
                        }),
                    Input('Price', this.priceController),
                    Input('Address', this.addressController),
                    Input('Bed', this.bedController),
                    Input('Bath', this.bathController),
                    Input('Parking', this.parkingController),
                    ValidationButton(() {
                      this.addProps(
                          this.widget.profile.id,
                          base64Image,
                          this.priceController.text,
                          this.addressController.text,
                          this.bedController.text,
                          this.bathController.text,
                          this.parkingController.text);
                    }),
                  ]),
            ),
          ),
          drawer: Menu(this.widget.profile));
    }
    return Container();
  }
}
