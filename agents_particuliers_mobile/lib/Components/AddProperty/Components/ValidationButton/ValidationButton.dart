import 'package:flutter/material.dart';

class ValidationButton extends StatelessWidget {
  ValidationButton(this.callBack);
  final callBack;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: FlatButton(
          onPressed: () {
            this.callBack();
          },
          child: Text(
            'Add',
            style: TextStyle(fontSize: 16),
          ),
          color: Color.fromRGBO(33, 8, 174, 1),
          textColor: Colors.white),
    );
  }
}
