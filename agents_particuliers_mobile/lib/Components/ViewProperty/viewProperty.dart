import 'package:agents_particuliers_mobile/Components/Profile/Profile.dart';
import 'package:agents_particuliers_mobile/Tools/Menu.dart';
import './Components/Overview/Overview.dart';
import './Components/Features/Features.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ViewProperty extends StatefulWidget {
  final Profile profile;

  ViewProperty(this.profile);

  String Price = "";
  String Addr = "";
  String Bed = "";
  String Bath = "";
  String Parking = "";
  Image image = null;

  void setAll(price, addr, bed, bath, parking, image) {
    this.Price = price;
    this.Addr = addr;
    this.Bed = bed;
    this.Bath = bath;
    this.Parking = parking;
    this.image = image;
  }

  @override
  _ViewPropertyState createState() => _ViewPropertyState();
}

class _ViewPropertyState extends State<ViewProperty> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Property")),
      drawer: Menu(this.widget.profile),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Overview(this.widget.Addr, this.widget.Bed, this.widget.Bath, this.widget.Parking, this.widget.Price),
            Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: Features(this.widget.image)),
          ],
        ),
      ),
    );
  }
}
