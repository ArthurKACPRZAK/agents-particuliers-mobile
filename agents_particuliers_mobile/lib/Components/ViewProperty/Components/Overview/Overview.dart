import 'package:flutter/material.dart';

class Overview extends StatelessWidget {

  Overview(this.Addr, this.Bed, this.Bath, this.Parking, this.Price);

  final String Addr;
  final String Bed;
  final String Bath;
  final String Parking;
  final String Price;

  @override
  Widget build(BuildContext context) {
    return
      Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage('Assets/studio.png'),
              fit: BoxFit.fill,
            ),
          ),
          height: 400.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  right: 15,
                  top: 15,
                ),
                child: Icon(
                  Icons.bookmark_border,
                  color: Colors.white,
                  size: 24.0,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 15.0, left: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(this.Price,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22,
                                color: Colors.white)),
                        Padding(
                            padding: const EdgeInsets.only(
                              left: 15,
                            ),
                            child: Text('Studio Apartment',
                                style: TextStyle(color: Colors.white)))
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10.0, left: 10),
                    child: Text(
                      this.Addr,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: Divider(
                      height: 20,
                      thickness: 1,
                      indent: 10,
                      endIndent: 10,
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 5,
                      bottom: 15,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          Icons.hotel,
                          color: Colors.white,
                          size: 24.0,
                        ),
                        Text(
                          this.Bed + ' Bed',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        Icon(
                          Icons.local_laundry_service,
                          color: Colors.white,
                          size: 24.0,
                        ),
                        Text(
                          this.Bath + ' Bath',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        Icon(
                          Icons.local_taxi,
                          color: Colors.white,
                          size: 24.0,
                        ),
                        Text(
                          this.Parking + ' parking',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ));
  }

}