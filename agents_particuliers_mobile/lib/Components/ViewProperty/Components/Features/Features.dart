import 'package:flutter/material.dart';

class Features extends StatelessWidget {

  Features(this.image);

final Image image;

  @override
  Widget build(BuildContext context) {
    return
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 20.0),
            height: 120.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    width: 120.0,
                    child: this.image != null
                        ? this.image
                        : Container()),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    width: 120.0,
                    child: this.image != null
                        ? this.image
                        : Container()),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    width: 120.0,
                    child: this.image != null
                        ? this.image
                        : Container()),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  width: 120.0,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage('Assets/studio.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  width: 120.0,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage('Assets/studio.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Text(
              'Features',
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('2 Movie Theatre'),
                Text('2 Km'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('4 Shopping Mall'),
                Text('1.3 Km'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('2 Top School'),
                Text('5 Km'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 10,
              bottom: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('2 Gov University'),
                Text('10 Km'),
              ],
            ),
          ),
          SizedBox(
            width: double.infinity,
            child: FlatButton(
                onPressed: () => {},
                height: 50,
                child: Text(
                  'Check Availability',
                  style: TextStyle(fontSize: 16),
                ),
                color: Color.fromRGBO(7, 165, 129, 1),
                textColor: Colors.white),
          ),
        ],
      );
  }

}